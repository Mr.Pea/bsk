package bsk.models.des;

import lombok.Getter;
import lombok.Setter;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;
import java.util.stream.Collectors;


public class DesCipherTest {

    @Test
    public void testWeakKey() {
        DesCipher cipher = new DesCipher();
        String key = "1111111000000001111111100000000111111110000000011111111000000001";
        String encrypted = cipher.encrypt(DesCipher.convertTextToBitsetText("12345678"), key);
        String decrypt = cipher.encrypt(encrypted, "0000000111111110000000011111111000000001111111100000000111111110");
        Assert.assertEquals("12345678", DesCipher.convertBitsetTextToText(decrypt));
    }

    @Test
    public void testEncryptDecrypt() {
        DesCipher cipher = new DesCipher();
        String key = "0001001100110100010101110111100110011011101111001101111111110001";
        String encrypted = cipher.encrypt(DesCipher.convertTextToBitsetText("12345678"), key);
        String decrypt = cipher.decrypt(encrypted, key);
        Assert.assertEquals("12345678", decrypt);
    }

    @Test
    public void testEncrypt1() {
        DesCipher cipher = new DesCipher();
        String key = "0001001100110100010101110111100110011011101111001101111111110001";
        String encrypt = cipher.encrypt(DesCipher.convertTextToBitsetText("DzienDob"), key);
        String decrypt = cipher.decrypt(encrypt, key);
        Assert.assertEquals("DzienDob", decrypt);
    }

    @Test
    public void testDecrypt1() {
        DesCipher cipher = new DesCipher();
        String key = "0001001100110100011101110111100110011011101111000001111111110001";
        String encrypt = cipher.encrypt(DesCipher.convertTextToBitsetText("Janek123"), key);
        String decrypt = cipher.decrypt(encrypt, key);
        Assert.assertEquals("Janek123", decrypt);
    }

    @Test
    public void testDecrypt2() {
        DesCipher cipher = new DesCipher();
        String key = "0001111100110100011101110111100110011011101111000001111111110001";
        String encrypt = cipher.encrypt(DesCipher.convertTextToBitsetText("DzienDobry"), key);
        String decrypt = cipher.decrypt(encrypt, key);
        Assert.assertEquals("DzienDobry", decrypt);
    }

    @Test
    public void testDecrypt3() {
        DesCipher cipher = new DesCipher();
        String key = "0001111100110100011101110111100110011011101111000001111111110001";
        String encrypt = cipher.encrypt(DesCipher.convertTextToBitsetText("Ale dziwny jest ten swiat"), key);
        System.out.println(encrypt.length());
        String decrypt = cipher.decrypt(encrypt, key);
        Assert.assertEquals("Ale dziwny jest ten swiat", decrypt);
    }

    @Test
    public void testEncrypt2() {
        String text = "0000000100100011010001010110011110001001101010111100110111101111";
        Perm perm = permutate(text);
        KeyGenerator keyGenerator = new KeyGenerator("0001001100110100010101110111100110011011101111001101111111110001");
        ArrayList<BitSet> byteArray = new ArrayList<>();
        while (true) {
            BitSet keyBlock;
            perm.next();
            if (perm.getRight() == null) {
                break;
            }
            for (int i = 1; i < 17; i++) {
                keyBlock = toBitSet(keyGenerator.generate());
                BitSet right = perm.getRight();
                String p = "32 1 2 3 4 5 4 5 6 7 8 9 8 9 10 11 12 13 12 13 14 15 16 17 16 17 18 19 20 21 20 21 22 23 24 25 24 25 26 27 28 29 28 29 30 31 32 1";
                right = (Perm.permutate(p, right));

                right.xor(keyBlock);

                List<BitSet> blockParts = splitBlock(right); //Rozwala na 8bitowe linie
                List<BitSet> converted = STable.permutate(blockParts);
                BitSet block = new BitSet();
                int index = 0;
                for (BitSet bitSet : converted) {
                    for (int j = 0; j < 4; j++) {
                        block.set(index++, bitSet.get(j));
                    }
                }
                Assert.assertTrue(block.length() <= 32);
                String p14 = "16 7 20 21 29 12 28 17 1 15 23 26 5 18 31 10 2 8 24 14 32 27 3 9 19 13 30 6 22 11 4 25";
                block = Perm.permutate(p14, block);
                block.xor(perm.getPrevLeft());
                perm.swap(block);
            }
            BitSet finalBlock = perm.getRight().get(0, 32);
            for (int i = 0; i < 32; i++) {
                finalBlock.set(i + 32, perm.getPrevLeft().get(i));
            }
            Assert.assertTrue(finalBlock.length() <= 64);
            String p18 = "40 8 48 16 56 24 64 32 39 7 47 15 55 23 63 31 38 6 46 14 54 22 62 30 37 5 45 13 53 21 61 29 36 4 44 12 52 20 60 28 35 3 43 11 51 19 59 27 34 2 42 10 50 18 58 26 33 1 41 9 49 17 57 25";
            finalBlock = Perm.permutate(p18, finalBlock);
            Assert.assertTrue(finalBlock.length() <= 64);
            byteArray.add(finalBlock);
        }

        Assert.assertEquals("1000010111101000000100110101010000001111000010101011010000000101", convertToBitString(byteArray));
    }

    private String convertToBitString(List<BitSet> blocks) {
        StringBuilder builder = new StringBuilder();
        for (BitSet block : blocks) {
            for (int i = 0; i < 64; i++) {
                builder.append(block.get(i) ? "1" : "0");
            }
        }
        return builder.toString();
    }

    private List<BitSet> splitBlock(BitSet set) {
        Assert.assertTrue(set.length() <= 48);
        List<BitSet> setList = new ArrayList<>(8);
        for (int i = 0; i < 8; i++) {
            BitSet part = set.get(i * 6, (i + 1) * 6);
            setList.add(part);
        }
        return setList;
    }


    private BitSet toBitSet(List<Boolean> generate) {
        BitSet set = new BitSet(generate.size());
        for (int i = 0; i < generate.size(); i++) {
            set.set(i, generate.get(i));
        }
        return set;
    }

    @Test
    public void testDecrypt() {

    }

    @Test
    public void testValidateKey() {
    }

    private Perm permutate(String text) {
        List<BitSet> byteSet = new ArrayList<>();
        int i;
        BitSet bitset = new BitSet();
        for (i = 0; i < text.length(); i++) {
            bitset.set(i, text.charAt(i) == '1');
        }
        byteSet.add(bitset);
        Perm perm = new Perm(byteSet);
        String p = "58 50 42 34 26 18 10 2 60 52 44 36 28 20 12 4 62 54 46 38 30 22 14 6 64 56 48 40 32 24 16 8 57 49 41 33 25 17 9 1 59 51 43 35 27 19 11 3 61 53 45 37 29 21 13 5 63 55 47 39 31 23 15 7";
        perm.permutate(p);
        return perm;
    }

    private static class Perm {
        private List<BitSet> set;
        private int blockIndex = 0;

        @Getter
        @Setter
        private BitSet left, right, rawRight, prevLeft;

        public Perm(List<BitSet> set) {
            this.set = set;
        }

        public void permutate(String p) {
            List<BitSet> bitSets = new ArrayList<>();
            for (BitSet set : set) {
                BitSet permuted = permutate(p, set);
                bitSets.add(permuted);
            }
            this.set = bitSets;
        }

        public static BitSet permutate(String p, BitSet set) {
            List<Integer> collect = Arrays.stream(p.split("\\s+")).map(Integer::parseInt).collect(Collectors.toList());
            BitSet newSet = new BitSet();
            for (int k = 0; k < collect.size(); k++) {
                newSet.set(k, set.get(collect.get(k) - 1));
            }
            for (int i = 0; i < collect.size(); i++) {
                Assert.assertEquals(set.get(collect.get(i) - 1), newSet.get(i));
            }
            return newSet;
        }

        public void next() {
            if (left != null && right != null) {
                blockIndex++;
            }
            if (set.size() == blockIndex) {
                left = null;
                right = null;
                return;
            }
            BitSet block = set.get(blockIndex);
            Assert.assertTrue(block.length() <= 64);
            BitSet lPart = block.get(0, 32);
            BitSet right = block.get(32, 64);
            this.rawRight = right;
            this.prevLeft = lPart;
            this.left = right;
            this.right = right;
        }

        public void swap(BitSet block) {
            prevLeft = left;
            left = block;
            right = block;
        }
    }
}