package bsk.models.stream_ciphers;

import bsk.models.Cipher;
import bsk.utils.BinaryConverter;

public class SynchronousStreamCipher extends Cipher {

    @Override
    public String encrypt(String plaintext, String key) {
        String binaryPlainText = BinaryConverter.convertToBinary(plaintext);
        String result = "";
        int keyIter = 0;
        for (int i = 0; i < binaryPlainText.length(); i++) {
            if (binaryPlainText.charAt(i) == ' ') {
                result += " ";
                continue;
            }

            if (keyIter > key.length() - 1) {
                keyIter = 0;
            }
            result += binaryPlainText.charAt(i) ^ key.charAt(keyIter);
            keyIter++;
        }
        return result;
    }

    @Override
    public String decrypt(String ciphertext, String key) {
        String result = "";
        int keyIter = 0;
        for (int i = 0; i < ciphertext.length(); i++) {
            if (ciphertext.charAt(i) == ' ') {
                result += " ";
                continue;
            }

            if (keyIter > key.length() - 1) {
                keyIter = 0;
            }
            result += key.charAt(keyIter) ^ ciphertext.charAt(i);
            keyIter++;
        }

        result = BinaryConverter.convertFromBinary(result);
        return result;
    }

    @Override
    public KeyError validateKey(String plaintext, String key) {

        if (!key.matches("^[0-1]{1,}")) {
            return KeyError.BINARY_ONLY;
        }
        return KeyError.CORRECT;
    }

    @Override
    public PlainTextError validatePlaintext(String plaintext, String key) {
        if (!plaintext.matches("[0-1 ]+")) {
            return PlainTextError.BINARY_ONLY;
        }
        return PlainTextError.CORRECT;
    }
}
