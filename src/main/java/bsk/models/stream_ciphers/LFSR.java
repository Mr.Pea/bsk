package bsk.models.stream_ciphers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class LFSR {
    private final Register register;
    private List<Integer> blocksToXOR = new ArrayList<>();
    List<Boolean> outputSequence;


    public List<Boolean> getOutputSequence() {
        return outputSequence;
    }

    public Register getRegister() {
        return register;
    }


    public LFSR(String functionString) {
        List<Boolean> function = parseFunction(functionString);
        register = new Register(function.size());

        for (int i = 1; i < function.size(); i++)
            if (function.get(i)) blocksToXOR.add(i - 1);

        outputSequence = generateOutputSequence();
    }


    public boolean nextBit() {
        return register.tick(blocksToXOR);
    }

    private List<Boolean> generateOutputSequence() {
        outputSequence = new ArrayList<>();
        Register startingRegister = new Register(register);
        nextBit();
        while (!startingRegister.equals(register)) {
            outputSequence.add(register.getBlocks().get(0));
            nextBit();
        }
        outputSequence.add(register.getBlocks().get(0));
        return outputSequence;
    }


    private List<Boolean> parseFunction(String function) {
        String lastCharAsString = String.valueOf(function.charAt(function.length() - 1));
        int len = lastCharAsString.charAt(0) == 'x' ? 2 : Integer.parseInt(lastCharAsString) + 1;
        Boolean[] booleans = new Boolean[len];
        Arrays.fill(booleans, Boolean.FALSE);

        String[] arr = function.split("\\+");
        for (String s : arr) {
            if (s.equals("1"))
                booleans[0] = true;
            else if (s.equals("x"))
                booleans[1] = true;
            else
                booleans[Integer.parseInt(String.valueOf(s.charAt(s.length() - 1)))] = true;
        }
        return Arrays.stream(booleans).collect(Collectors.toList());
    }

    public static class Register {
        private List<Boolean> blocks = new ArrayList<>();
        private final Random rand = new Random();
        private boolean result;

        public Register(int size) {
            if (size > 2) {
                for (int i = 0; i < size - 1; i++)
                    blocks.add(rand.nextBoolean());
                while (!(blocks.stream().distinct().count() == 2))
                    randomizeBlocks();
            } else blocks.add(true);
        }

        public Register(Register register) {
            blocks = new ArrayList<>(register.getBlocks());
            result = register.result;
        }

        public List<Boolean> getBlocks() {
            return blocks;
        }

        public void setResult(boolean result) {
            this.result = result;
        }

        private void randomizeBlocks() {
            for (int i = 0; i < blocks.size(); i++) {
                blocks.set(i, rand.nextBoolean());
            }
        }

        private void shift(boolean Q1Value) {
            if (blocks.size() > 1) {
                for (int i = blocks.size() - 1; i >= 1; i--)
                    blocks.set(i, blocks.get(i - 1));
                blocks.set(0, Q1Value);
            }
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Register)
                return ((Register) obj).getBlocks().equals(blocks);
            return false;
        }

        private boolean tick(List<Integer> blocksToXOR) {
            boolean Q1Value;
            if (blocksToXOR.size() == 1) {
                Q1Value = blocks.get(blocksToXOR.get(0));
            } else
                Q1Value = blocks.get(blocksToXOR.get(0)) ^ blocks.get(blocksToXOR.get(1));

            if (blocksToXOR.size() > 2)
                for (int i = 2; i < blocksToXOR.size(); i++) {
                    Q1Value = Q1Value ^ blocks.get(blocksToXOR.get(i));
                }

            shift(Q1Value);
            result = Q1Value;
            return Q1Value;
        }

        public String getResult() {
            return result ? "1" : "0";
        }
    }

    public String getOutputSequenceAsString() {
        return outputSequence.stream().map(value -> value ? "1" : "0").collect(Collectors.joining());
    }
}
