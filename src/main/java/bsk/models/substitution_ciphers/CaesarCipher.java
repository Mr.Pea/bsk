package bsk.models.substitution_ciphers;

import bsk.models.Cipher;

public class CaesarCipher extends Cipher {

    private static final int FIRST_CHAR = 97;
    private static final int NUMBER_OF_CHARS = 26;
    private static final int FIRST_UPPER_CHAR = 65;
    private static final int LAST_UPPER_CHAR = 90;


    @Override
    public String encrypt(String plaintext, String key) {
        int keyValue = Integer.parseInt(key);
        char[] newCharset = new char[plaintext.length()];
        char[] charArray = plaintext.toCharArray();
        boolean isUpper = false;
        for (int i = 0; i < charArray.length; i++) {
            char c = charArray[i];
            if (c == 32) {
                newCharset[i] = ' ';
                continue;
            }
            if (c >= FIRST_UPPER_CHAR && c <= LAST_UPPER_CHAR) {
                isUpper = true;
                c += FIRST_CHAR - FIRST_UPPER_CHAR;
            }
            newCharset[i] = (char) (((c - FIRST_CHAR + keyValue) % (NUMBER_OF_CHARS)) + FIRST_CHAR);
            if (isUpper) {
                newCharset[i] -= (FIRST_CHAR - FIRST_UPPER_CHAR);
                isUpper = false;
            }
        }
        return new String(newCharset);
    }

    @Override
    public String decrypt(String ciphertext, String key) {
        int keyValue = Integer.parseInt(key);
        keyValue = NUMBER_OF_CHARS - keyValue;
        return encrypt(ciphertext, String.valueOf(keyValue));
    }

    @Override
    public KeyError validateKey(String plaintext, String key) {
        if (!key.matches("([0-9])*")) {
            return KeyError.NUMBER_ONLY;
        }
        if (Integer.parseInt(key) > 26) {
            return KeyError.TOO_BIG_NUMBER_26;
        }
        return KeyError.CORRECT;
    }
}
