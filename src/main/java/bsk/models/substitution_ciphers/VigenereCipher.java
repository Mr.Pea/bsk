package bsk.models.substitution_ciphers;

import bsk.models.Cipher;

public class VigenereCipher extends Cipher {

    private char[][] cipherTable = new char['Z' + 1]['Z' + 1];
    String result = "";

    @Override
    public String encrypt(String plaintext, String key) {
        key = key.toUpperCase();
        plaintext = plaintext.toUpperCase();
        fillInCipherTable();

        for (int i = 0; i < plaintext.length(); i++) {
            if (plaintext.charAt(i) != ' ') {
                result += cipherTable[key.charAt(i % (key.length()))][plaintext.charAt(i)];
            } else result += ' ';

        }
        return result;
    }

    @Override
    public String decrypt(String ciphertext, String key) {
        key = key.toUpperCase();
        ciphertext = ciphertext.toUpperCase();
        fillInCipherTable();
        for (int i = 0; i < ciphertext.length(); i++) {
            if (ciphertext.charAt(i) != ' ') {
                for (int j = 'A'; j <= 'Z'; j++) {
                    if (ciphertext.charAt(i) == cipherTable[key.charAt(i % key.length())][j]) {
                        result += cipherTable['A'][j];
                    }
                }
            } else {
                result += ' ';
            }
        }
        return result;
    }

    @Override
    public KeyError validateKey(String plaintext, String key) {
        if (!key.matches("^[a-zA-Z\\s]*$")) {
            return KeyError.LETTERS_ONLY;
        } else return KeyError.CORRECT;
    }

    @Override
    public PlainTextError validatePlaintext(String plaintext, String key) {
        if (!plaintext.matches("^[a-zA-Z\\s]*$")) {
            return PlainTextError.LETTERS_ONLY;
        } else return PlainTextError.CORRECT;
    }

    public void fillInCipherTable() {
        for (char i = 'A'; i <= 'Z'; i++) {
            char pos = i;
            for (int j = 'A'; j <= 'Z'; j++) {
                if (pos == 'Z' + 1) {
                    pos = 'A';
                }
                cipherTable[i][j] = pos;
                pos++;
            }
        }
    }
}
