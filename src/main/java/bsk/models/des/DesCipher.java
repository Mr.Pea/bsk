package bsk.models.des;

import bsk.models.Cipher;
import lombok.Getter;
import lombok.Setter;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class DesCipher extends Cipher {

    private static final String EBIT = "32 1 2 3 4 5 4 5 6 7 8 9 8 9 10 11 12 13 12 13 14 15 16 17 16 17 18 19 20 21 20 21 22 23 24 25 24 25 26 27 28 29 28 29 30 31 32 1";
    private static final String P = "16 7 20 21 29 12 28 17 1 15 23 26 5 18 31 10 2 8 24 14 32 27 3 9 19 13 30 6 22 11 4 25";
    private static final String IP_1 = "40 8 48 16 56 24 64 32 39 7 47 15 55 23 63 31 38 6 46 14 54 22 62 30 37 5 45 13 53 21 61 29 36 4 44 12 52 20 60 28 35 3 43 11 51 19 59 27 34 2 42 10 50 18 58 26 33 1 41 9 49 17 57 25";
    private static final String IP = "58 50 42 34 26 18 10 2 60 52 44 36 28 20 12 4 62 54 46 38 30 22 14 6 64 56 48 40 32 24 16 8 57 49 41 33 25 17 9 1 59 51 43 35 27 19 11 3 61 53 45 37 29 21 13 5 63 55 47 39 31 23 15 7";

    @Override
    public String encrypt(String plaintext, String key) {
        BitSet bitSet = castToBitset(plaintext);
        List<BitSet> bitSets = runDes(bitSet, plaintext.length(), key, KeyGenerator.GeneratorType.ENCRYPT_ORDER);
        return convertToBitString(bitSets);
    }

    private static BitSet convertToBitSet(String plaintext) {
        return BitSet.valueOf(plaintext.getBytes());
    }

    @Override
    public String decrypt(String ciphertext, String key) {
        BitSet bitSet = castToBitset(ciphertext);
        List<BitSet> bitSets = runDes(bitSet, ciphertext.length(), key, KeyGenerator.GeneratorType.DECRYPT_ORDER);
        return convertToText(bitSets);
    }

    public static String convertTextToBitsetText(String text) {
        return convertToBitString((convertToBitSet(text)));
    }
    public static String convertBitsetTextToText(String text) {
        return convertToText(Collections.singletonList(castToBitset(text)));
    }

    public static String convertToText(List<BitSet> bitSets) {
        StringBuilder builder = new StringBuilder();
        for (BitSet bitSet : bitSets) {
            builder.append(new String(bitSet.toByteArray(), StandardCharsets.US_ASCII));
        }
        return builder.toString();
    }

    private static BitSet castToBitset(String ciphertext) {
        BitSet set = new BitSet();
        for (int i = 0; i < ciphertext.length(); i++) {
            set.set(i, ciphertext.charAt(i) == '1');
        }
        return set;
    }

    private List<BitSet> runDes(BitSet input, int length, String key, KeyGenerator.GeneratorType generatorType) {
        DesHelper desHelper = initialize(input, length);
        desHelper.permutate(IP);
        List<BitSet> blocks = new ArrayList<>();
        while (true) {
            Iterator<List<Boolean>> keyGenerator = new KeyGenerator(key).getIterator(generatorType);
            desHelper.next();
            if (desHelper.getRight() == null) {
                break;
            }
            for (int i = 0; i < 16; i++) {
                BitSet keyBlock = toBitSet(keyGenerator.next());
                BitSet right = desHelper.getRight();
                right = (DesHelper.permutate(EBIT, right));

                right.xor(keyBlock);

                List<BitSet> blockParts = splitTo6BitBlocks(right);
                List<BitSet> sBlock = STable.permutate(blockParts);
                int index = 0;
                for (BitSet bitSet : sBlock) {
                    for (int j = 0; j < 4; j++) {
                        right.set(index++, bitSet.get(j));
                    }
                }
                right = DesHelper.permutate(P, right);
                right.xor(desHelper.getPrevLeft());
                desHelper.swap(right);
            }
            BitSet finalBlock = desHelper.getRight().get(0, 32);
            for (int i = 0; i < 32; i++) {
                finalBlock.set(i + 32, desHelper.getPrevLeft().get(i));
            }
            finalBlock = DesHelper.permutate(IP_1, finalBlock);
            blocks.add(finalBlock);
        }
        return blocks;
    }

    @Override
    public KeyError validateKey(String plaintext, String key) {
        if (key.length() != 64) {
            return KeyError.DES_KEY_LENGTH;
        }
        for (char c : key.toCharArray()) {
            if (c != '1' && c != '0') {
                return KeyError.DES_KEY_FORMAT;
            }
        }
        return KeyError.CORRECT;
    }

    private static String convertToBitString(BitSet block) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < block.length(); i++) {
            builder.append(block.get(i) ? "1" : "0");
        }
        return builder.toString();
    }

    private String convertToBitString(List<BitSet> blocks) {
        StringBuilder builder = new StringBuilder();
        for (BitSet block : blocks) {
            for (int i = 0; i < 64; i++) {
                builder.append(block.get(i) ? "1" : "0");
            }
        }
        return builder.toString();
    }

    private DesHelper initialize(BitSet set, int length) {
        int blocksNumber = length / 64;
        if (length % 64 != 0) {
            blocksNumber++;
        }
        List<BitSet> byteSet = new ArrayList<>();
        for (int i = 0; i < blocksNumber; i++) {
            BitSet bitSet = new BitSet();
            int index = 64 * i;
            for (int j = 0; j < 64; j++) {
                if (length <= index) {
                    bitSet.set(index, length, false);
                    continue;
                }
                bitSet.set(j, set.get(index));
                index++;
            }
            byteSet.add(bitSet);
        }
        return new DesHelper(byteSet);
    }

    private List<BitSet> splitTo6BitBlocks(BitSet set) {
        List<BitSet> setList = new ArrayList<>(8);
        for (int i = 0; i < 8; i++) {
            BitSet part = set.get(i * 6, (i + 1) * 6);
            setList.add(part);
        }
        return setList;
    }


    private BitSet toBitSet(List<Boolean> generate) {
        BitSet set = new BitSet(generate.size());
        for (int i = 0; i < generate.size(); i++) {
            set.set(i, generate.get(i));
        }
        return set;
    }

    private static class DesHelper {
        private List<BitSet> set;
        private int blockIndex = 0;

        @Getter
        @Setter
        private BitSet left, right, prevLeft;

        public DesHelper(List<BitSet> set) {
            this.set = set;
        }

        public void permutate(String p) {
            List<BitSet> bitSets = new ArrayList<>();
            for (BitSet set : set) {
                BitSet permuted = permutate(p, set);
                bitSets.add(permuted);
            }
            this.set = bitSets;
        }

        public static BitSet permutate(String p, BitSet set) {
            List<Integer> collect = Arrays.stream(p.split("\\s+")).map(Integer::parseInt).collect(Collectors.toList());
            BitSet newSet = new BitSet();
            for (int k = 0; k < collect.size(); k++) {
                newSet.set(k, set.get(collect.get(k) - 1));
            }
            return newSet;
        }

        public void next() {
            if (left != null && right != null) {
                blockIndex++;
            }
            if (set.size() == blockIndex) {
                left = null;
                right = null;
                return;
            }
            BitSet block = set.get(blockIndex);
            BitSet lPart = block.get(0, 32);
            BitSet right = block.get(32, 64);
            this.prevLeft = lPart;
            this.left = right;
            this.right = right;
        }

        public void swap(BitSet block) {
            prevLeft = left;
            left = block;
            right = block;
        }
    }
}
