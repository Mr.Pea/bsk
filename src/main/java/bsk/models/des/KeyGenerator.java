package bsk.models.des;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class KeyGenerator implements Iterable<List<Boolean>> {

    public enum GeneratorType {
        ENCRYPT_ORDER, DECRYPT_ORDER
    }

    private List<Boolean> key;
    private List<Boolean> leftPart;
    private List<Boolean> rightPart;
    private List<Integer> pc1;
    private List<Integer> pc2;
    private List<Integer> shifts;
    private int iteration = 0;

    private String pc1Raw = "57 49 41 33 25 17 9\n" +
            "1 58 50 42 34 26 18\n" +
            "10 2 59 51 43 35 27\n" +
            "19 11 3 60 52 44 36\n" +
            "63 55 47 39 31 23 15\n" +
            "7 62 54 46 38 30 22\n" +
            "14 6 61 53 45 37 29\n" +
            "21 13 5 28 20 12 4";

    private String pc2Raw = "14 17 11 24 1 5\n" +
            "3 28 15 6 21 10\n" +
            "23 19 12 4 26 8\n" +
            "16 7 27 20 13 2\n" +
            "41 52 31 37 47 55\n" +
            "30 40 51 45 33 48\n" +
            "44 49 39 56 34 53\n" +
            "46 42 50 36 29 32";


    private String shiftsRaw = "1 1 2 2 2 2 2 2 1 2 2 2 2 2 2 1";

    public KeyGenerator(String key) {
        this.key = parse(key);
        pc1 = prepare(pc1Raw);
        pc2 = prepare(pc2Raw);
        shifts = prepare(shiftsRaw);
        this.key = permuteKey(pc1);
        leftPart = this.key.subList(0, this.key.size() / 2);
        rightPart = this.key.subList(this.key.size() / 2, this.key.size());
    }

    private List<Boolean> permuteKey(List<Integer> lookupTable) {
        List<Boolean> newKey = new ArrayList<>();
        lookupTable.forEach(val -> newKey.add(key.get(val - 1)));
        key = newKey;
        return newKey;

    }

    private void leftShift(List<Boolean> list) {
        for (int j = 0; j < shifts.get(iteration); j++) {
            boolean tmp = list.get(0);
            for (int i = 1; i < list.size(); i++)
                list.set(i - 1, list.get(i));
            list.set(list.size() - 1, tmp);
        }
    }

    private List<Boolean> parse(String key) {
        key = key.replace(" ", "");
        return key.chars().mapToObj(c -> (char) c == '1').collect(Collectors.toList());
    }

    private List<Integer> prepare(String raw) {
        raw = raw.replace("\n", " ");
        String[] tmp = raw.split(" ");
        return Arrays.stream(tmp).map(Integer::valueOf).collect(Collectors.toList());
    }

    public List<Boolean> generate() {
        leftShift(leftPart);
        leftShift(rightPart);
        key = new ArrayList<>();
        key.addAll(leftPart);
        key.addAll(rightPart);
        key = permuteKey(pc2);
        iteration++;
        return key;
    }

    public List<Boolean> getKey() {
        return key;
    }

    public String getKeyAsString() {
        return key.stream().map(aBoolean -> aBoolean ? "1" : "0").collect(Collectors.joining(""));
    }

    public Iterator<List<Boolean>> getIterator(GeneratorType type) {
        switch (type) {
            case DECRYPT_ORDER:
                return reverseIterator();
            case ENCRYPT_ORDER:
                return iterator();
            default:
                return null;
        }
    }

    @Override
    public Iterator<List<Boolean>> iterator() {
        return new Iterator<List<Boolean>>() {
            private int index = 0;

            @Override
            public boolean hasNext() {
                return index != 16;
            }

            @Override
            public List<Boolean> next() {
                index++;
                return generate();
            }
        };
    }

    public Iterator<List<Boolean>> reverseIterator() {
        return new Iterator<List<Boolean>>() {
            int index = 15;
            private final List<List<Boolean>> keys = new ArrayList<>();

            {
                for (int i = 0; i < 16; i++) {
                    keys.add(generate());
                }
            }

            @Override
            public boolean hasNext() {
                return index >= 0;
            }

            @Override
            public List<Boolean> next() {
                return keys.get(index--);
            }
        };
    }
}
