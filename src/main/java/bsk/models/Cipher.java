package bsk.models;

import bsk.models.des.DesCipher;
import bsk.models.substitution_ciphers.CaesarCipher;
import bsk.models.substitution_ciphers.VigenereCipher;
import bsk.models.transposition_ciphers.ColumnarNumber;
import bsk.models.transposition_ciphers.ColumnarWordIncompleteRows;
import bsk.models.transposition_ciphers.RailFence;
import bsk.models.transposition_ciphers.ColumnarWordCompleteRows;
import lombok.Getter;

public abstract class Cipher {
    public abstract String encrypt(String plaintext, String key);

    public abstract String decrypt(String ciphertext, String key);

    public abstract KeyError validateKey(String plaintext, String key);

    public enum CipherType {
        RAIL_FENCE("Rail fence transposition"),
        COLUMNAR_NUMBER("Columnar transposition with key X-X-X-X"),
        COLUMNAR_WORD_COMPLETE_ROWS("Columnar transposition with text key, complete rows variant"),
        COLUMNAR_WORD_INCOMPLETE_ROWS("Columnar transposition with text key, incomplete rows variant"),
        CAESAR_CIPHER("Caesar cipher"),
        VIGENERE_CIPHER("Vigenere cipher"),
        DES_CIPHER("DES cipher");

        @Getter
        private String value;

        CipherType(String s) {
            this.value = s;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public enum KeyError {
        KEY_TOO_SHORT("Key value must have at least 2 characters"),
        KEY_VALUE_TOO_BIG("Key value shouldn't be bigger than length of plaintext!"),
        WRONG_FORMAT("Key has wrong format!"),
        NUMBER_DASH_ONLY("Key must contains only number or '-'!"),
        NUMBER_NOT_CONSISTENT("Numbers should be complete!"),
        CORRECT("Provided key is correct."),
        KEY_VALUE_TOO_SMALL("Key value must be equal or bigger than 2!"),
        KEY_INADEQUATE("Key ability to encrypt is not good enough! Try other!"),
        NUMBER_ONLY("Key must be a number"),
        LETTERS_ONLY("Key must only have a letters with no space"),
        BINARY_ONLY("Key must be in binary format number"),
        TOO_BIG_NUMBER_26("Number must be lower or equal to 26"),
        DES_KEY_LENGTH("Key length must be exactly 64 bit"),
        DES_KEY_FORMAT("Only 0,1 key value allowed");

        @Getter
        private String value;

        KeyError(String s) {
            this.value = s;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public enum PlainTextError {
        LETTERS_ONLY("Input text must only have a letters with no space"),
        BINARY_ONLY("Key must be in binary format number"),
        CORRECT("Provided plaintext is correct.");

        @Getter
        private String value;

        PlainTextError(String s) {
            this.value = s;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public static Cipher createFromType(CipherType type) {
        if (type == null) {
            throw new NullPointerException("Cipher type is null");
        }
        switch (type) {
            case RAIL_FENCE:
                return new RailFence();
            case COLUMNAR_WORD_COMPLETE_ROWS:
                return new ColumnarWordCompleteRows();
            case COLUMNAR_NUMBER:
                return new ColumnarNumber();
            case COLUMNAR_WORD_INCOMPLETE_ROWS:
                return new ColumnarWordIncompleteRows();
            case CAESAR_CIPHER:
                return new CaesarCipher();
            case VIGENERE_CIPHER:
                return new VigenereCipher();
            case DES_CIPHER:
                return new DesCipher();
        }
        throw new RuntimeException(String.format("Cipher %s does not exists", type.toString()));
    }

    public PlainTextError validatePlaintext(String plaintext, String key) {
        return PlainTextError.CORRECT;
    }
}
