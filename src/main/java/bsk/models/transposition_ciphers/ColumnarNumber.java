package bsk.models.transposition_ciphers;

import bsk.models.Cipher;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ColumnarNumber extends Cipher {

    @Override
    public String decrypt(String ciphertext, String key) {
        List<Integer> collect = Arrays.stream(key.split("-"))
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        Integer[] decryptKey = new Integer[collect.size()];
        for (int i = 0; i < collect.size(); i++) {
            Integer integer = collect.get(i);
            decryptKey[integer - 1] = i + 1;
        }

        return doAlgorithm(ciphertext.toCharArray(), Arrays.asList(decryptKey));
    }

    @Override
    public KeyError validateKey(String plaintext, String key) {
        String[] numbersS = key.split("-");
        if (numbersS.length <= 1) {
            return KeyError.WRONG_FORMAT;
        }
        Integer[] numbers = new Integer[numbersS.length];
        for (String s : numbersS) {
            if (!s.matches("([0-9])")) {
                return KeyError.NUMBER_DASH_ONLY;
            }
            int number = Integer.parseInt(s);
            if (numbers.length < number) {
                return KeyError.NUMBER_NOT_CONSISTENT;
            }
        }
        return KeyError.CORRECT;
    }

    @Override
    public String encrypt(String plaintext, String key) {
        List<Integer> collect = Arrays.stream(key.split("-")).map(Integer::parseInt).collect(Collectors.toList());
        return doAlgorithm(plaintext.replaceAll("\\s+", "_").toCharArray(), collect);
    }

    private String doAlgorithm(char[] source, List<Integer> key) {
        int max = key.stream().max(Integer::compareTo).orElseGet(() -> {
            throw new RuntimeException("Unexpected behavior");
        });
        int size = source.length;
        if (source.length % max != 0) {
            size += (max - source.length % max);
        }
        char[] charset = new char[size];
        for (int line = 0, pos = 0; line <= source.length / max; line++) {
            for (int i = 0; i < key.size() && pos < size; i++) {
                Integer integer = key.get(i);
                int index = line * max + (integer - 1);
                if (index >= source.length) {
                    charset[pos++] = ' ';
                    continue;
                }
                charset[pos++] = source[index];
            }
        }
        return new String(charset);
    }
}
