package bsk.models.transposition_ciphers;


import bsk.models.Cipher;

import java.util.*;

public class RailFence extends Cipher {

    @Override
    public KeyError validateKey(String plaintext, String key) {
        if (!key.matches("\\d+")) return KeyError.WRONG_FORMAT;
        else if (plaintext.length() <= Integer.parseInt(key)) return KeyError.KEY_VALUE_TOO_BIG;
        else if (Integer.parseInt(key) == 1) return KeyError.KEY_VALUE_TOO_SMALL;
        else return KeyError.CORRECT;
    }

    @Override
    public String encrypt(String plaintext, String key) {
        int keyInt = Integer.parseInt(key);
        StringBuilder[] table = new StringBuilder[keyInt];
        for (int i = 0; i < keyInt; i++) {
            table[i] = new StringBuilder();
        }
        int row = 0;
        boolean down = true;
        plaintext = plaintext.replaceAll("\\s+", "_");
        for (char c : plaintext.toCharArray()) {
            table[row].append(c);
            if (down) row++;
            else row--;
            if (row == keyInt - 1) down = false;
            else if (row == 0) down = true;
        }
        StringBuilder ciphertextBuilder = new StringBuilder();
        for (StringBuilder r : table) {
            ciphertextBuilder.append(r.toString());
        }
        return ciphertextBuilder.toString();
    }

    @Override
    public String decrypt(String ciphertext, String key) {
        int keyInt = Integer.parseInt(key);
        int cycleLength = keyInt * 2 - 2;
        double numberOfCycles = ciphertext.length() * 1.0 / cycleLength;
        List<Queue<Character>> table = new ArrayList<>();
        for (int i = 0; i < keyInt; i++)
            table.add(new LinkedList<>());
        int start = 0;
        int end = (int) Math.ceil(numberOfCycles);
        for (int i = 0; i < keyInt; i++) {
            for (int x = start; x < end; x++) {
                table.get(i).add(ciphertext.charAt(x));
            }
            start = end;
            if (i == keyInt - 2) {
                end += numberOfCycles;
            } else end += 2 * Math.floor(numberOfCycles);
            if (ciphertext.length() % Math.floor(numberOfCycles) >= i + 2)
                end += 1;
        }
        int length = 0;
        boolean down = true;
        int row = 0;

        StringBuilder plaintextBuilder = new StringBuilder();
        while (length++ != ciphertext.length()) {
            plaintextBuilder.append(table.get(row).poll());
            if (down) row++;
            else row--;
            if (row == keyInt - 1) down = false;
            else if (row == 0) down = true;
        }

        return plaintextBuilder.toString();
    }
}
