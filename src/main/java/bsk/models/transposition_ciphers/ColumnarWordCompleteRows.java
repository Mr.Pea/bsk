package bsk.models.transposition_ciphers;

import bsk.models.Cipher;

import java.util.Arrays;
import java.util.HashMap;

public class ColumnarWordCompleteRows extends ColumnarWord {

    @Override
    public String encrypt(String plaintext, String key) {
        int columnNumber = key.length();
        plaintext = plaintext.replaceAll("\\s+", "_");
        int rowNumber = plaintext.length() / key.length();
        if (plaintext.length() % key.length() != 0) {
            rowNumber++;
        }

        String encryptedMessage = "";
        char[] sortedKey = sortKey(key);
        HashMap<Integer, Integer> order = new HashMap<>();
        createOrder(sortedKey, order, key);

        char[][] messageContainer = new char[rowNumber][columnNumber];
        int counter = 0;

        for (int i = 0; i < rowNumber; i++) {
            for (int j = 0; j < columnNumber; j++) {
                if (counter < plaintext.length()) {
                    messageContainer[i][j] = plaintext.charAt(counter);
                    counter++;
                } else break;
            }
        }

        for (int i = 0; i < columnNumber; i++) {

            for (int j = 0; j < rowNumber; j++) {
                if (messageContainer[j][order.get(i)] != '0') {
                    encryptedMessage += messageContainer[j][order.get(i)];
                } else break;
            }
            if (i != columnNumber - 1)
                encryptedMessage += " ";
        }
        return encryptedMessage;
    }


    @Override
    public String decrypt(String ciphertext, String key) {
        int columnNumber = key.length();
        int rowNumber = ciphertext.length() / key.length();
        if (ciphertext.length() % key.length() != 0) {
            rowNumber++;
        }

        String decryptedMessage = "";
        char[] sortedKey = sortKey(key);
        HashMap<Integer, Integer> order = new HashMap<>();
        createOrder(sortedKey, order, key);

        char[][] messageContainer = new char[rowNumber][columnNumber];
        int counter = 0;


        for (int i = 0; i < columnNumber; i++) {
            for (int j = 0; j < rowNumber; j++) {
                if (counter < ciphertext.length() && ciphertext.charAt(counter) != ' ') {
                    messageContainer[j][order.get(i)] = ciphertext.charAt(counter);
                    counter++;
                } else {
                    counter++;
                    break;
                }
            }
        }

        for (int i = 0; i < rowNumber; i++) {
            for (int j = 0; j < columnNumber; j++) {
                if (messageContainer[i][j] == '_') {
                    decryptedMessage += " ";
                } else
                    decryptedMessage += messageContainer[i][j];
            }
        }
        return decryptedMessage;
    }

}
