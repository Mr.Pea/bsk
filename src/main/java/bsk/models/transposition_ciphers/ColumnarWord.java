package bsk.models.transposition_ciphers;

import bsk.models.Cipher;

import java.util.Arrays;
import java.util.HashMap;

public abstract class ColumnarWord extends Cipher {
    public abstract String encrypt(String plaintext, String key);

    public abstract String decrypt(String ciphertext, String key);

    @Override
    public KeyError validateKey(String plaintext, String key) {
        if (!key.matches("^[a-zA-Z\\s]*$")) return KeyError.WRONG_FORMAT;
        else if (key.length() < 2) return KeyError.KEY_TOO_SHORT;
        else return KeyError.CORRECT;
    }

    protected void createOrder(char[] sortedKey, HashMap<Integer, Integer> order, String key) {
        for (int i = 0; i < sortedKey.length; i++) {
            for (int j = 0; j < sortedKey.length; j++) {
                if (sortedKey[i] == key.charAt(j)) {
                    if (!order.containsValue(j)) {
                        order.put(i, j);
                        break;
                    }
                }
            }
        }
    }

    protected char[] sortKey(String str) {
        char[] arr = str.toCharArray();
        Arrays.sort(arr);
        return arr;
    }
}
