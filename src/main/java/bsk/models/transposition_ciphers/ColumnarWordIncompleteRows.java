package bsk.models.transposition_ciphers;

import bsk.models.Cipher;

import java.util.*;

public class ColumnarWordIncompleteRows extends ColumnarWord {
    @Override
    public String encrypt(String plaintext, String key) {
        plaintext = plaintext.replaceAll("\\s+", "_");
        int columns = key.length();
        char[] sortedKey = sortKey(key);
        HashMap<Integer, Integer> order = new HashMap<>();
        List<List<Character>> matrix = new LinkedList<>();
        for (int i = 0; i < columns; i++)
            matrix.add(new LinkedList<>());

        createOrder(sortedKey, order, key);
        fillWithOrder(plaintext, order, matrix);

        StringBuilder cipherText = new StringBuilder();

        int col = 0;
        while (col != columns) {
            for (List<Character> characterList : matrix) {
                if (characterList.size() >= order.get(col) + 1) {
                    cipherText.append(characterList.get(order.get(col)));
                }
            }
            col++;
        }
        return cipherText.toString();
    }

    private void fillWithOrder(String text, HashMap<Integer, Integer> order, List<List<Character>> matrix) {
        int start = 0;
        int end = order.get(0);
        int currentRow = 0;
        while (getAbsoluteSize(matrix) != text.length()) {
            int finalCurrentRow = currentRow;
            text.substring(start, end + 1).chars().forEach(character -> matrix.get(finalCurrentRow).add((char) character));
            currentRow++;
            start = end + 1;
            end = order.get(currentRow) + start;
            if (end > text.length()) end = text.length() - 1;
        }
    }

    private int getAbsoluteSize(List<List<Character>> matrix) {
        return matrix.stream().mapToInt(List::size).sum();
    }

    @Override
    public String decrypt(String ciphertext, String key) {
        int columns = key.length();
        char[] sortedKey = sortKey(key);
        HashMap<Integer, Integer> order = new HashMap<>();
        List<List<Character>> template = new LinkedList<>();
        for (int i = 0; i < columns; i++)
            template.add(new LinkedList<>());

        createOrder(sortedKey, order, key);
        fillWithOrder(ciphertext, order, template);


        char[][] matrix = new char[columns][columns];

        int charPos = 0;
        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < columns; j++) {
                if (!(template.get(j).size() < order.get(i) + 1))
                    matrix[j][order.get(i)] = ciphertext.charAt(charPos++);
            }
        }

        StringBuilder plaintext = new StringBuilder();
        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < columns; j++) {
                if (matrix[i][j] != '\u0000')
                    plaintext.append(matrix[i][j]);
            }
        }
        return plaintext.toString();
    }

    @Override
    public KeyError validateKey(String plaintext, String key) {
        char[] sortedKey = sortKey(key);
        HashMap<Integer, Integer> order = new HashMap<>();
        createOrder(sortedKey, order, key);
        Cipher.KeyError error = super.validateKey(plaintext, key);
        if (error == KeyError.CORRECT) {
            if (order.values().stream().reduce(0, Integer::sum) < plaintext.length())
                error = KeyError.KEY_INADEQUATE;
        }
        return error;
    }
}
