package bsk;

import bsk.controllers.Controller;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    public void start(Stage primaryStage) throws Exception {
        Controller controller = new Controller();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("Main.fxml"));
        fxmlLoader.setController(controller);
        Parent parent = fxmlLoader.load();

        primaryStage.setTitle("Bsk 1");
        primaryStage.setScene(new Scene(parent, 800, 600));
        primaryStage.show();

        primaryStage.setOnCloseRequest(e -> {
            primaryStage.close();
            System.exit(0);
        });
    }

    public static void main(String[] args) {
        launch(Main.class);
    }

}
