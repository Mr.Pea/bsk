package bsk.utils;

public class BinaryConverter {

    public static String convertToBinary(String s) {
        int size = s.length();
        String out = "";
        for (int i = 0; i < size; i++)
        {
            int val = Integer.valueOf(s.charAt(i));

            String bin = "";
            while (val > 0)
            {
                if (val % 2 == 1)
                {
                    bin += '1';
                }
                else {
                    bin += '0';
                }
                val /= 2;
            }
            bin = reverse(bin);
            bin = checkBits(bin);
            out += bin + " ";
        }
        return out;
    }

    public static String convertFromBinary(String string) {
        String[] words = string.split(" ");
        String out = "";
        for(String s:words) {
            out += (char) Integer.parseInt(s, 2);
        }
        return out;
    }

    private static String reverse(String string)
    {
        char[] a = string.toCharArray();
        int l, r = 0;
        r = a.length - 1;

        for (l = 0; l < r; l++, r--)
        {
            char temp = a[l];
            a[l] = a[r];
            a[r] = temp;
        }
        return String.valueOf(a);
    }

    private static String checkBits(String string) {
        StringBuilder sb = new StringBuilder(string);
        while(sb.toString().length() < 7) {
            sb.insert(0, "0");
        }
        return sb.toString();
    }
}
