package bsk.controllers;

import bsk.models.Cipher;
import bsk.models.des.DesCipher;
import bsk.models.stream_ciphers.LFSR;
import bsk.models.stream_ciphers.SynchronousStreamCipher;
import bsk.utils.BinaryConverter;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Controller {
    @FXML
    private Text result;
    @FXML
    private TableView<LFSR.Register> lfsrTable;
    @FXML
    private TextField functionText;

    @FXML
    private TextField cipherKeyStream;

    @FXML
    private TextField functionInput;

    @FXML
    private Button generateLFSR;
    @FXML
    private AnchorPane root;
    @FXML
    private Button startStop;
    @FXML
    private ChoiceBox<Cipher.CipherType> cipherChoice;

    @FXML
    private TextArea inputText;

    @FXML
    private TextArea outputText;

    @FXML
    private TextArea outputTextStream;

    @FXML
    private TextArea inputTextStream;

    @FXML
    private TextField cipherKey;

    @FXML
    private Button encryptButton;

    @FXML
    private CheckBox binaryInput;

    @FXML
    private Button showAsText;

    public Controller() {
    }

    @FXML
    private void initialize() {
        cipherChoice.setItems(FXCollections.observableArrayList(Cipher.CipherType.values()));
        cipherChoice.getSelectionModel().selectedItemProperty().addListener((observableValue, oldItem, newItem) -> {
            showAsText.setDisable(true);
            boolean show = false;
            if (Cipher.CipherType.DES_CIPHER.equals(newItem)) {
                show = true;
            }
            showAsText.setVisible(show);
            binaryInput.setVisible(show);
        });
    }

    @FXML
    private void onEncryptClick(ActionEvent event) {
        Cipher cipher = getCipher();
        if (cipher == null) return;
        Cipher.KeyError keyError = cipher.validateKey(inputText.getText(), cipherKey.getText());
        Cipher.PlainTextError plainTextError = cipher.validatePlaintext(inputText.getText(), cipherKey.getText());
        if (keyError != Cipher.KeyError.CORRECT) {
            showAlertDialog("Validation error", "Key is incorrect", keyError.toString());
            return;
        }

        if (plainTextError != Cipher.PlainTextError.CORRECT) {
            showAlertDialog("Validation error", "Plaintext is incorrect", plainTextError.toString());
            return;
        }
        String inputText = this.inputText.getText();
        if (!binaryInput.isSelected()) {
            inputText = DesCipher.convertTextToBitsetText(inputText);
        }
        String encrypt = cipher.encrypt(inputText, cipherKey.getText());

        outputText.setText(encrypt);
        showAsText.setDisable(false);
    }

    @FXML
    private void onDecryptClick(ActionEvent event) {
        Cipher cipher = getCipher();
        if (cipher == null) return;

        String decrypt = cipher.decrypt(inputText.getText(), cipherKey.getText());
        Cipher.KeyError keyError = cipher.validateKey(inputText.getText(), cipherKey.getText());
        if (keyError != Cipher.KeyError.CORRECT) {
            showAlertDialog("Validation error", "Key is incorrect", keyError.toString());
            return;
        }

        Cipher.PlainTextError plainTextError = cipher.validatePlaintext(inputText.getText(), cipherKey.getText());
        if (plainTextError != Cipher.PlainTextError.CORRECT) {
            showAlertDialog("Validation error", "Cipher text is incorrect", plainTextError.toString());
            return;
        }
        outputText.setText(decrypt);
        showAsText.setDisable(false);
    }

    private Cipher getCipher() {
        Cipher.CipherType selectedItem = cipherChoice.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            showAlertDialog("Error", "Empty field", "Please choose one of the cipher");
            return null;
        }
        Cipher cipher = Cipher.createFromType(selectedItem);

        if (isEmpty(inputText.getText())) {
            showAlertDialog("Error", "Empty field", "Please insert input data");
            return null;
        }
        if (isEmpty(cipherKey.getText())) {
            showAlertDialog("Error", "Empty field", "Please insert key");
            return null;
        }
        return cipher;
    }

    private boolean isEmpty(String input) {
        return input.trim().length() == 0;
    }

    @FXML
    private void onLoadFromFileClick(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        File file = chooser.showOpenDialog(root.getScene().getWindow());
        if (file == null) {
            return;
        }
        Scanner fin;
        StringBuilder sc = new StringBuilder();
        try {
            fin = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            showAlertDialog("Error", "File not found", e.getLocalizedMessage().trim());
            return;
        }

        while (fin.hasNextLine()) {
            sc.append(fin.nextLine());
            sc.append("\n");
        }

        fin.close();
        inputText.setText(sc.toString());
    }

    @FXML
    private void onLoadFromFileStreamClick(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        File file = chooser.showOpenDialog(root.getScene().getWindow());
        if (file == null) {
            return;
        }
        Scanner fin;
        StringBuilder sc = new StringBuilder();
        try {
            fin = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            showAlertDialog("Error", "File not found", e.getLocalizedMessage().trim());
            return;
        }

        while (fin.hasNextLine()) {
            sc.append(fin.nextLine());
            sc.append("\n");
        }

        fin.close();
        inputTextStream.setText(sc.toString());
    }

    @FXML
    private void onSaveToFIleClick(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        File file = chooser.showSaveDialog(root.getScene().getWindow());
        if (file == null) {
            return;
        }

        FileWriter fout;
        try {
            fout = new FileWriter(file);
            fout.write(outputText.getText());
            fout.close();
        } catch (IOException e) {
            e.printStackTrace();
            showAlertDialog("Error", "Unexpected error :O", e.getLocalizedMessage().trim());
            return;
        }
    }

    @FXML
    private void onSaveToFIleStreamClick(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        File file = chooser.showSaveDialog(root.getScene().getWindow());
        if (file == null) {
            return;
        }

        FileWriter fout;
        try {
            fout = new FileWriter(file);
            fout.write(outputTextStream.getText());
            fout.close();
        } catch (IOException e) {
            e.printStackTrace();
            showAlertDialog("Error", "Unexpected error :O", e.getLocalizedMessage().trim());
            return;
        }
    }

    private void showAlertDialog(String title, String headerText, String contentText) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText(contentText);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.show();
    }

    LFSR lfsr = null;

    @FXML
    private void onGenerateLFSRClick(ActionEvent event) {
        String fn = functionText.getText();
        if (fn.matches("^(1\\+)(x)*(x\\+)?(x\\^\\d+\\+)*(x\\^\\d+)*$")) {
            lfsrTable.getItems().clear();
            lfsrTable.getColumns().clear();
            lfsrTable.refresh();
            lfsrTable.setVisible(true);
            lfsr = new LFSR(fn);
            LFSR.Register startingRegister = new LFSR.Register(lfsr.getRegister());
            lfsr.nextBit();
            for (int i = 0; i < startingRegister.getBlocks().size(); i++) {
                TableColumn<LFSR.Register, String> col = new TableColumn<>("Q".concat(String.valueOf(i)));
                int finalI = i;
                col.setCellValueFactory(p -> new SimpleStringProperty(p.getValue().getBlocks().get(finalI) ? "1" : "0"));
                col.setSortable(false);
                lfsrTable.getColumns().add(col);
            }
            TableColumn<LFSR.Register, String> col = new TableColumn<>("Result");
            col.setCellValueFactory(p -> new SimpleStringProperty(p.getValue().getResult()));
            col.setSortable(false);
            lfsrTable.getColumns().add(col);
            while (lfsrTable.getItems().size() != lfsr.getOutputSequence().size()) {
                LFSR.Register register = new LFSR.Register(lfsr.getRegister());
                register.setResult(lfsr.nextBit());
                lfsrTable.getItems().add(register);
            }
            lfsrTable.setFixedCellSize(25);
            lfsrTable.prefHeightProperty().bind(Bindings.size(lfsrTable.getItems()).multiply(lfsrTable.getFixedCellSize()).add(30));
            startStop.setDisable(false);
        } else {
            showAlertDialog("Error", "Validation error", "provided function doesn't match with pattern");
        }
    }

    @FXML
    private void onGenerateClick(ActionEvent event) {
        String fn = functionInput.getText();
        if (fn.matches("^(1\\+)(x)*(x\\+)?(x\\^\\d+\\+)*(x\\^\\d+)*$")) {
            LFSR lfsr = new LFSR(fn);
            cipherKeyStream.setText(lfsr.getOutputSequenceAsString());
        } else {
            showAlertDialog("Error", "Validation error", "provided function doesn't match with pattern");
        }
    }

    @FXML
    private void onEncryptStreamClick(ActionEvent event) {
        Cipher cipher = new SynchronousStreamCipher();
        if (cipher == null) return;
        Cipher.KeyError keyError = cipher.validateKey(inputTextStream.getText(), cipherKeyStream.getText());

        if (isEmpty(inputTextStream.getText())) {
            showAlertDialog("Error", "Empty field", "Please insert input data");
            return;
        }
        if (isEmpty(cipherKeyStream.getText())) {
            showAlertDialog("Error", "Empty field", "Please insert key");
            return;
        }

        if (keyError != Cipher.KeyError.CORRECT) {
            showAlertDialog("Validation error", "Key is incorrect", keyError.toString());
            return;
        }

        String encrypt = cipher.encrypt(inputTextStream.getText(), cipherKeyStream.getText());

        outputTextStream.setText(encrypt);
    }

    @FXML
    private void onDecryptStreamClick(ActionEvent event) {
        Cipher cipher = new SynchronousStreamCipher();
        if (cipher == null) return;

        if (isEmpty(inputTextStream.getText())) {
            showAlertDialog("Error", "Empty field", "Please insert input data");
            return;
        }
        if (isEmpty(cipherKeyStream.getText())) {
            showAlertDialog("Error", "Empty field", "Please insert key");
            return;
        }

        Cipher.KeyError keyError = cipher.validateKey(inputTextStream.getText(), cipherKeyStream.getText());
        if (keyError != Cipher.KeyError.CORRECT) {
            showAlertDialog("Validation error", "Key is incorrect", keyError.toString());
            return;
        }

        Cipher.PlainTextError plainTextError = cipher.validatePlaintext(inputTextStream.getText(), cipherKeyStream.getText());
        if (plainTextError != Cipher.PlainTextError.CORRECT) {
            showAlertDialog("Validation error", "Cipher text is incorrect", plainTextError.toString());
            return;
        }

        String decrypt = cipher.decrypt(inputTextStream.getText(), cipherKeyStream.getText());
        outputTextStream.setText(decrypt);
    }

    Timer timer = null;

    @FXML
    private void onStartStopClick(ActionEvent event) {
        if (timer == null) {
            ActionListener taskPerformer = actionEvent -> {
                lfsrTable.getSelectionModel().select(lfsr.getRegister());
                result.setText(lfsr.nextBit() ? "1" : "0");
                result.setUnderline(!result.underlineProperty().get());
            };
            timer = new Timer(1500, taskPerformer);
            timer.setRepeats(true);
            timer.start();
        } else if (timer.isRunning()) timer.stop();
        else timer.start();
    }

    @FXML
    private void showOutputAsText(ActionEvent event) {
        if (outputText.getText().length() == 0) {
            return;
        }
        String text = DesCipher.convertBitsetTextToText(outputText.getText());
        outputText.setText(text);
    }
}
